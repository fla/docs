# Déframasoftiser Framasphère

Afin de [déframasoftiser Internet](https://framablog.org/2019/09/24/deframasoftisons-internet/) voici comment exporter vos données depuis nos services.

## Exporter

Pour exporter vos données, vous devez&nbsp;:

  * cliquer sur votre avatar dans le coin supérieur droit
  * cliquer sur **[Paramètres](https://framasphere.org/user/edit)**
  * cliquer sur le bouton **Demander mes données de profil** dans la section **Exporter des données**
  * un mail avec un lien pour télécharger directement vos données vous est envoyé (pensez à vérifier vos spams) ; vous pouvez aussi rafraîchir la page pour voir apparaître le bouton **Télécharger mon profil**
  * télécharger le fichier avec l'extension `.json.gz` sur votre ordinateur
  * cliquer sur le bouton **Demander mes photos** sur cette même page **[Paramètres](https://framasphere.org/user/edit)**
  * cliquer sur le bouton **Télécharger mes photos** après le rechargement de la page
  * télécharger le fichier avec l'extension `.zip` sur votre ordinateur.
