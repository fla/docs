# Déframasoftiser Framasite wiki

Afin de [déframasoftiser Internet](https://framablog.org/2019/09/24/deframasoftisons-internet/) voici comment exporter vos données depuis nos services.

## Export du wiki

Pour exporter votre wiki vous devez, **en tant qu'administratrice du wiki**&nbsp;:

  1. cliquer sur **Administrer**
  * cliquer sur **DokuWiki Advanced: Export Utility** dans la section **Extensions**
  * cocher sur **Include sub-namespaces** pour inclure toutes les sous-pages
  * cliquer sur le bouton **Export all Pages in Namespace →**

Le téléchargement d'un dossier compressé `DokuWiki-export--20191025-100200.zip` contenant votre wiki commencera.

<div class="alert-warning alert">
<b>Cette méthode n'exporte pas les médias</b> ! Pour cela, vous devez utiliser la méthode décrite <a href="#export-complet">ci-dessous</a>.
</div>

## Export complet

Pour exporter l'**intégralité** de votre wiki (incluant les médias - ce que ne fait pas l'outil d'export ci-dessus), pour le déplacer sur un serveur, vous devez, **en tant qu'administratrice du wiki**&nbsp;:

  1. cliquer sur **Administrer**
  * cliquer sur **Outil de sauvegarde**
  * [optionnel] (dé)sélectionner des critères
  * cliquer sur le bouton **Créer la sauvegarde**

    ![dokuwiki exporter sauvegarde](images/dokuwiki_export_complet_selection.png)

  * cliquer sur le lien de la sauvegarde pour récupérer sur votre ordinateur le fichier compressé `.tar`

    ![dokuwiki exporter lien](images/dokuwiki_export_complet_recuperation.png)

Le fichier `.tar` doit être décompressé avec un logiciel comme ceux listés sur [Framalibre](https://framalibre.org/tags/compression-de-fichiers) par exemple.

<div class="alert-warning alert">
Il semblerait que 7zip ait des difficultés avec les archives tar générées par Dokuwiki. Pour palier à cela vous pouvez soit utiliser la <a href="https://www.7-zip.org/">toute dernière version du logiciel (alpha)</a> soit utiliser un autre logiciel.
</div>

## Import du wiki

Sur une instance **DokuWiki**, pour **importer** un wiki, vous devez&nbsp;:

  1. cliquer sur **Administrer**
  * cliquer sur **DokuWiki Advanced: Import Utility** dans la section **Extensions**
  * cliquer sur le bouton **Parcourir…** pour récupérer l'archive zip (au format `DokuWiki-export--20191025-100200.zip`) sur votre ordinateur
  * cliquer sur **Upload backup file →**&nbsp;:
    1. laissez la valeur par défaut
    * sélectionnez les pages que vous souhaitez importer ou cochez la case à gauche de **File** pour les importer toutes
    * **Optionnellement** vous pouvez cocher **Overwrite existing pages** si vous souhaitez écraser les pages déjà existantes sur votre wiki : si vous venez de créer le wiki pour importer un nouveau, ce n'est pas nécessaire (puisqu'il n'y a rien à écraser) ; si vous avez déjà un wiki, attention&nbsp;: l'import supprimera les pages existantes
  * cliquer sur le bouton **Import**

Votre wiki est alors importé.
