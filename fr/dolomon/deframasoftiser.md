# Déframasoftiser Framaclic

<div role="alert" aria-live="polite" aria-atomic="true" class="alert alert-dismissible alert-warning" id="f-alert-deframa"><div><p><strong>Déframasoftisons Internet</strong> : l’association Framasoft a programmé la
fermeture de certains services, pour éviter d’épuiser nos maigres ressources
et pour favoriser l’utilisation de petits hébergeurs de proximité (<a href="https://framablog.org/2020/03/03/10-bonnes-raisons-de-fermer-certains-services-framasoft-la-5e-est-un-peu-bizarre/">toutes les infos ici</a>).
<br><b class="violet">Frama</b><b class="vert">clic</b> a fermé <strong>mardi 12 janvier 2021</strong>.<br>
Nous avons réuni des outils pour vous aider à récupérer vos données
et à trouver un service similaire <a href="https://alt.framasoft.org/fr/framaclic">sur cette page</a>.</p></div></div>

## Export

Pour exporter vos données de Framaclic, vous devez&nbsp;:

  1. cliquer sur votre identifiant
  * cliquer sur [**<i class="fa fa-exchange" aria-hidden="true"></i> Exporter ou importer des données**](https://framaclic.org/export-import) en haut à droite de l'interface
  * cliquer sur le bouton **Exporter vos données**
  * cliquer sur le lien disponible sur la page ou sur celui disponible dans le mail qui vous a été envoyé
  * enregistrer le fichier `.json` sur votre ordinateur

## Import

Pour importer les données depuis un service comme Framaclic vers une autre instance, vous devez&nbsp;:

  1. cliquer sur votre identifiant en haut à droite de l'interface
  * cliquer sur le bouton **Parcourir…** dans la section **Import de données**
  * récupérer le fichier `.json` exporté de votre ancienne instance
  * cliquer sur le bouton **Importer des données** (un mail vous sera envoyé pour vous confirmer l'action)

## Supprimer votre compte Framaclic

Pour supprimer votre compte Framaclic avant la fermeture du service, vous devez&nbsp;:

  1. cliquer sur
  * cliquer sur [**<i class="fa fa-cog" aria-hidden="true"></i> Modifier les détails du compte**](https://framaclic.org/user)
  * entrer votre mot de passe dans le champ **Supprimer mon compte**
  * cliquer sur le bouton **Supprimer mon compte**
