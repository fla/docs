# Framaclic

<div class="alert alert-warning">
<b class="violet">Frama</b><b class="vert">clic</b> a fermé ses portes le <a href="https://framablog.org/2020/03/03/10-bonnes-raisons-de-fermer-certains-services-framasoft-la-5e-est-un-peu-bizarre/">mardi 12 janvier 2021</a>. Vous trouverez un service similaire <a href="https://alt.framasoft.org/fr/framaclic">sur cette page</a>.<br>
</div>

[Framaclic](https://framaclic.org) est un raccourcisseur d'URL qui compte les clics.

![ajout dolo](images/ajoutDoloVFVVOK.png)

Vous pouvez découvrir les fonctionnalités et utilisations de Framaclic en consultant notre [exemple d'utilisation](exemple-d-utilisation.md).

## Pour aller plus loin :

* Un [exemple d'utilisation](exemple-d-utilisation.md)
* [Déframasoftiser Internet](deframasoftiser.html)
* ~~Testez [Framaclic](https://framaclic.org/)~~
    * (optionnel) le [plugin Dolomon pour WordPress](https://fr.wordpress.org/plugins/dolomon/)
* Installez [Dolomon sur votre propre serveur](https://framacloud.org/fr/cultiver-son-jardin/dolomon.html)
* Contribuez [au code de Dolomon](https://framagit.org/luc/dolomon)
* [Découvrez et soutenez le travail de Luc sur son blog](https://fiat-tux.fr/)
