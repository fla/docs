Calendriers
========

il existe deux visualisations différentes des calendriers :

- La vue du projet avec des filtres (disponibles depuis le tableau)
- La vue utilisateur (disponible depuis le tableau de bord de l'utilisateur)

Pour l'instant le calendrier permet d'afficher les informations suivantes :

- Les tâches avec une date d'échéance, affichée en haut. **La date d'échéance peut être modifiée en déplaçant la tâche vers un autre jour**.
- les tâches basées sur la date de création ou la date de début. **Ces évènements ne peuvent pas être modifiés avec le calendrier**.
- Le suivi dans le temps de sous-tâches, tous les segments temporels sont affichés dans le calendrier.
- Les estimations pour les sous-tâches, les prévisions et le travail restant

![Calendrier](screenshots/calendar.png)

La configuration du calendrier peut être modifiée dans la page des paramètres.

Remarque : la date d'échéance n'inclut pas d'information temporelle.

## Abonnements iCal

Il est possible de visualiser les tâches de Framaboard depuis Framagenda (sans synchronisation) ou depuis Thunderbird (avec mise à jour).

  * Les calendriers seront en **Lecture seule**, c'est-à-dire que vous ne pourrez pas les modifier depuis Framagenda ou Thunderbird : seulement les voir (ce qui est déjà bien, non ?).
  * Seules les tâches comprises entre 2 mois avant la date actuelle et 6 mois après seront importées.
  * Les projets doivent être en accès **public** : vous devez aller dans **Préférences** > **Accès public** > **Activer l'accès public**.

Pour récupérer le lien `iCal` à importer, vous devez aller **Préférences** du projet puis copier le lien **Abonnement iCal** dans la section **Résumé**. Celui est de la forme `https://PROJET.framaboard.org/?controller=ICalendarController&action=project&token=SERIE-DE-CHIFFRES-ET-LETTRES`

### Dans Framagenda

Pour Framagenda vous devez suivre la procédure expliquée [dans la documentation de Framagenda](/agenda/Interface-Agenda.html#abonnement-à-des-calendriers).

**Note** : Les abonnements ne sont pas synchronisables avec un client CalDAV.

### Dans Thunderbird

Pour Thunderbird, vous devez suivre la procédure expliquée [dans la documentation de Framagenda](/agenda/Synchronisation/Thunderbird.html#synchronisation-des-agendas).
