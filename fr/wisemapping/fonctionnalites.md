# Fonctionnalités

## Raccourcis

Vous pouvez voir les raccourcis pour Framindmap avec compte sur la page : https://framindmap.org/c/keyboard

## Interface

![image de l'interface](images/mindmap_interface.png)

1. **Enregistrer** pour enregistrer la version actuelle de votre carte
* **Imprimer** pour imprimer votre carte (voir aussi [La carte s’imprime en tout petit, comment améliorer ça ?](https://contact.framasoft.org/fr/faq/#mindmap_impression))
* **Annuler** pour annuler la dernière action réalisée sur la carte
* **Refaire** pour revenir à la dernière action réalisée et annulée par l'action **Annuler**
* **Réduire l'affichage** pour dézoomer sur la carte
* **Agrandir l'affichage** pour zoomer sur la carte
* **Forme du nœud** pour changer la forme d'un nouveau nœud
* **Ajouter un nœud** pour ajouter un nœud à celui sélectionné
* **Supprimer un nœud** pour supprimer le nœud sélectionné
* **Couleur de la bordure du nœud** pour modifier la couleur de la bordure du nœud sélectionné
* **Couleur du nœud** pour modifier la couleur du fond du nœud sélectionné
* **Ajouter une icône** pour ajouter une icône dans le nœud sélectionné
* **Ajouter une note** pour insérer une note visible en passant la souris sur l'icône dans le nœud
* **Ajouter un lien** pour insérer une lien visible en passant la souris sur l'icône dans le nœud
* **Relation du nœud** pour ajouter des flèches entre deux nœuds
* **Type de la police** pour modifier l'aspect de la police d'écriture du nœud sélectionné
* **Taille de la police** pour modifier la taille de la police d'écriture du nœud sélectionné
* **Caractères gras** pour donner un aspect gras à la police d'écriture du nœud sélectionné
* **Caractères italiques** pour donner un aspect italique à la police d'écriture du nœud sélectionné
* **Couleur de police** pour modifier la couleur de la police d'écriture du nœud sélectionné
* **Partager** pour [partager la carte avec un (ou plusieurs) compte Framindmap](#partager-une-carte)
* **Publier** pour [publier publiquement une carte](#publier-une-carte)
* **Exporter** pour [exporter une carte](#exporter-une-carte)
* **Historique** pour afficher les versions enregistrer de la carte (avec possibilité de visionner la version ou de la restaurer)

## Publier une carte

<div class="alert-info alert">
Publier une carte signifie qu'elle sera visible par toute personne possédant le lien.
</div>

Pour publier une carte mentale vous devez :

* cliquer sur l'icône **Publier** : ![icône Publier](images/mindmap_publier.png)
* cocher **Activer le partage**

Si vous souhaitez *embarquer* la carte sur un site, vous devez alors copier le code du style :

```
<iframe style="width:600px;height:400px;border: 1px
solid black" src="https://framindmap.org/c/maps/CHIFFRES/embed?zoom=1"> </iframe>
```
Sinon, vous pouvez cliquer sur l'onglet **URL Publique** et copier l'url de votre carte pour la donner aux personnes souhaitées.

N'oubliez pas de cliquer sur **Accepter** pour confirmer la publication de votre carte.

Pour ne plus partager la carte vous devez&nbsp;:

  1. cliquer sur l'icône **Publier** : ![icône Publier](images/mindmap_publier.png)
  * cocher **Activer le partage**
  * cliquer sur le bouton **Accepter**

## Enregistrer une carte sur son ordinateur

Vous devez :
  * cliquer sur l'icône **Exporter**  
  ![icone Exporter mindmap](images/wisemapping_sauvegarde.png)
  * sélectionner le format souhaité
  * cliquer sur **Accepter**

## Exporter et importer une carte

Vous pouvez exporter des cartes pour les importer dans un autre compte ou sur une autre instance utilisant le logiciel **Wisemapping** ou acceptant les formats compatibles.

Le logiciel utilisé par Framindmap avec compte permet d'importer les cartes aux formats **FreeMind 1.0.1** et **WiseMapping**.

### Exporter une carte

Vous devez&nbsp;:

  1. cliquer sur l'icône **Exporter**  
  ![icone Exporter mindmap](images/wisemapping_sauvegarde.png)
  * sélectionner le format **Freeplane/Freemind v1.0.1 (.mm)** ou **WiseMapping (.wxml)**
  * cliquer sur **Accepter**

### Importer une carte

Vous devez&nbsp;

  1. sur la page listant vos cartes, cliquer sur l'icône <i class="fa fa-arrow-circle-o-up" aria-hidden="true"></i> **Importer**
  * cliquer sur le bouton **Parcourir…** pour récupérer la carte sur votre ordinateur (voir ci-dessus)
  * facultatif : donner un nom et une description
  * cliquer sur le bouton **Importer**

<div class="alert-info alert">Vous devrez recréer les partages éventuels.</div>

## Partager une carte

Pour partager une carte, il faut que toutes les personnes aient un compte Framindmap. Sur la carte que vous souhaitez partager&nbsp;:

  * cliquez sur l'icône ![icone partage framindmap](images/wisemapping_partage_icon.png)
  * entrez l'adresse mail du compte (**1**)
  * optionnel : passez de la possibilité pour la personne d'éditer la carte (**Peut éditer**) à **Peut regarder**
  * cliquez sur **Ajouter** (**2**) pour envoyer un mail de partage à l'adresse ajoutée
  * cliquez sur **Accepter** (**3**)
    ![image procédure partage framindmap](images/wisemapping_partager.png)

## Renommer une carte

Pour renommer une carte vous devez cocher la case devant son nom dans [la liste de vos cartes](https://framindmap.org/c/maps/) puis&nbsp;:

![gif animé montrant la procédure pour renommer une carte](images/mindmap_rename.gif)

  1. cliquer sur **Plus**
  * cliquer sur **Renommer**
  * changer le nom de la carte
  * cliquer sur le bouton **Renommer**

## Retour à la ligne dans un nœud

Vous devez faire `ctrl`+`Entrée` pour aller à la ligne (`Entrée` simplement enregistre le nœud).
