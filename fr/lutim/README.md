# Framapic

<div class="alert-warning alert">
<b class="violet">Frama</b><b class="vert">pic</b> a fermé ses portes le <a href="https://framablog.org/2020/03/03/10-bonnes-raisons-de-fermer-certains-services-framasoft-la-5e-est-un-peu-bizarre/">mardi 12 janvier 2021</a>. Vous trouverez un service similaire <a href="https://alt.framasoft.org/fr/framapic">sur cette page</a>.<br>
</div>

[Framapic](https://framapic.org) est un service en ligne libre qui permet de partager des images de manière confidentielle et sécurisée.

  1. Collez l’image à transmettre.
  2. Si besoin, définissez sa durée de conservation en ligne.
  3. Partagez ensuite avec vos correspondants le lien qui vous est donné.

Vos images sont chiffrées et stockées sur nos serveurs sans qu’il nous soit possible de les déchiffrer.

Le service repose sur le logiciel libre [Lutim](https://lut.im/)¹.

Grâce à une nouvelle fonctionnalité ajoutée par son développeur, [Framapic](https://framapic.org) vous permet désormais de créer aisément une galerie d'images&nbsp;! Découvrez comment dans notre [exemple d'utilisation](galerie.md).

¹ : attention, cela se prononce «&nbsp;lutin&nbsp;» pas «&nbsp;lut**ime**&nbsp;»

---

## Tutoriel vidéo

<div class="text-center">
  <p><video controls="controls" preload="none" poster="https://framatube.org/images/media/989l.jpg" height="340" width="570">
      <source src="https://framatube.org/blip/framapic.mp4" type="video/mp4">
      <source src="https://framatube.org/blip/framapic.webm" type="video/webm">
  </video></p>
  <p>→ La <a href="https://framatube.org/blip/framapic.webm">vidéo au format webm</a></p>
</div>

Vidéo réalisée par [arpinux](http://arpinux.org/), artisan paysagiste de la distribution GNU/Linux pour débutant [HandyLinux](https://handylinux.org/)

## Pour aller plus loin&nbsp;:
  * [Déframasoftiser Internet](deframasoftiser.html)
  * ~~[Essayer Framapic](https://framapic.org)~~
  * [Prise en main](prise-en-main.md)
  * [Créer une galerie photos](galerie.md)
  * Application Android&nbsp;:
    * [Goblim](https://f-droid.org/packages/fr.mobdev.goblim/) sur F-droid
    * [Goblim](https://play.google.com/store/apps/details?id=fr.mobdev.goblim) sur Google Play
  * [Dégooglisons Internet](https://degooglisons-internet.org)
  * [Soutenir Framasoft](https://soutenir.framasoft.org)
