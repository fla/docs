# Prise en main

<div class="alert-warning alert">
<b class="violet">Frama</b><b class="vert">pic</b> a fermé <a href="https://framablog.org/2020/03/03/10-bonnes-raisons-de-fermer-certains-services-framasoft-la-5e-est-un-peu-bizarre/">mardi 12 janvier 2021</a>. Vous trouverez un service similaire <a href="https://alt.framasoft.org/fr/framapic">sur cette page</a>.<br>
L’envoi d’images est dorénavant désactivé, mais vous pouvez toujours récupérer vos images sur la page <a href="https://framapic.org/myfiles">Mes images</a>.
</div>

## Supprimer une photo

Pour supprimer une photo que vous avez versé sur Framapic vous devez, **depuis le même navigateur que celui utilisé pour l'envoi de l'image**&nbsp;:

  1. aller dans l'onglet **Mes images**
  * cliquer sur l'icône <i class="fa fa-trash" aria-hidden="true"></i>

## Modifier un date d'expiration

Pour modifier une date d'expiration vous devez, **depuis le même navigateur que celui utilisé pour l'envoi de l'image**, aller dans l'onglet **Mes images**, puis&nbsp;:

  1. cliquer sur l'icône <i class="fa fa-pencil-square-o" aria-hidden="true"></i>
  * changer la date
  * cliquer sur **Enregistrer les modifications**

![image montrant comment éditer une date](images/lutim_edit_date_fr.png)
