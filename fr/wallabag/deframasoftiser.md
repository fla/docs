# Déframasoftiser Framabag

Afin de [déframasoftiser Internet](https://framablog.org/2019/09/24/deframasoftisons-internet/) voici comment exporter vos données depuis nos services.

<div class="alert-info alert">Framabag a définitivement fermé ses portes le <b>06 juillet 2021</b>.</div>

## Exporter

Pour exporter tous vos articles Framabag, vous devez cliquer sur l'icône ![icône de téléchargement des articles](images/download_article.png) depuis la page **Tous les articles**.

Si vous ne souhaitez exporter que les articles non lus, c'est à partir de l'onglet **Non lus** que vous devez cliquer sur l'icône ![icône de téléchargement des articles](images/download_article.png) etc…

Vous pouvez télécharger vos articles dans plusieurs formats : ePUB, MOBI, PDF, XML, JSON, CSV.

## Importer

Pour importer vos articles dans une autre instance Wallabag, vous devez&nbsp;:
 1. cliquer sur **Importer** dans le menu latéral gauche
 * cliquer sur **Importer les contenus** dans la section **wallabag v2**
 * cliquer sur le bouton **Fichier** pour récupérer le fichier d'export ([voir ci-dessus](#exporter))
 * sélectionner votre fichier
 * cliquer sur **Importer le fichier**

<div class="alert-info alert">
Si vous utilisez une application et/ou une extension Wallabag, pensez à mettre à jour les informations&nbsp;!
</div>
