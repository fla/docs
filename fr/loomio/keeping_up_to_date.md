# Se tenir à jour

## Paramètres des courriels

Vos paramètres de courriels vous permettent de définir la fréquence à laquelle vous avez des nouvelles d‎‎’un groupe ou d‎‎’un fil de discussion particulier. Il existe trois paramètres valables à la fois pour les groupes et les fils de discussion.

### Paramètres de la messagerie pour les groupes

Pour modifier vos paramètres de courriel de groupe, vous devez cliquer sur l'onglet **Paramètres** de votre groupe puis sur **Préférences de courriel**.

* **Dès qu'il y a de l'activité (Abonné)**&nbsp;: vous recevrez un courriel à chaque fois qu‎‎’il y a de l‎‎’activité (commentaires, votes, nouveaux fils, propositions, et résultats de proposition) dans ce groupe. c'est le paramètre par défaut pour les nouveaux groupes.

* **Lorsque quelqu'un réclame votre attention (Normal)**&nbsp;: vous recevrez un courriel au sujet des nouveaux fils et propositions (c‎‎’est-à-dire lorsqu‎‎’une proposition est créée, s‎‎’apprête à fermer ou quand un résultat est créé pour une proposition).

*  **Pas de notification (Ignorer)**&nbsp;: vous ne recevrez aucun courriel portant sur l‎‎’activité dans ce groupe, mais vous pourrez voir les dernières mises à jour sur les pages [**Non lues**]((#reading_loomio.html#unread-threads) et la page **Récent**.

Pour appliquer les mêmes paramètres à l‎‎’ensemble de vos groupes, cochez la case **Appliquer à tous mes groupes** avant d‎‎’envoyer le formulaire.

### Paramètres de la messagerie d‎‎’un fil de discussion

![image de modification de notification pour un fil de discussion](images/edit_fil_notifications.png)

Lorsque vous commencez un nouveau fil de discussion, il hérite des paramètres de courriel par défaut attribués par son groupe. Pour modifier les paramètres de courriel, ouvrez le u fil de discussion et sélectionnez l'onglet **Se désinscrire** (ou **S'inscrire** si vous vous abonnez ou avez été invité)

* **Dès qu'il y a de l'activité (Abonné)**&nbsp;: vous recevrez un courriel à chaque fois qu‎‎’il y a de l‎‎’activité (commentaires, nouvelles propositions, votes et résultats d‎‎’une proposition) dans ce fil.

* **Lorsque quelqu'un réclame votre attention (Normal)**&nbsp;: vous recevrez un courriel lorsqu‎‎’une proposition est créée, s‎‎’apprête à fermer ou lorsqu‎‎’un résultat est créé.

* **Pas de notification (Ignorer)**&nbsp;: vous ne recevrez aucun courriel au sujet de ce fil.

Si vous avez ignorer les notifications vous ne serez pas notifié concernant les propositions d'un fil, ou toute autre activité sur ce fil, sauf si quelqu'un vous @mentionne.

### Répondre par mail

Vous pouvez répondre aux commentaires que vous recevez directement par mail et vos réponses seront postées dans le fil Framavox. Vous pouvez aussi choisir de voir le commentaire dans le fil Framavox directement en cliquant sur **Répondre à cette discussion** dans le mail.

<div class="alert-warning alert">
Attention à bien supprimer le contenu du mail avant d'envoyer votre réponse.
</div>
