# Premiers pas

Il existe plusieurs façons de s‎‎’initier à Loomio. Voici quelques informations sur chaque point d‎‎’entrée disponible&nbsp;:

## Se créer un compte

Pour vous créer un compte sur Framavox vous devez&nbsp;:

  1. entrer votre adresse mail sur https://framavox.org/ dans la fenêtre **Identifiez-vous sur Framavox** qui apparaît quand vous visitez la page (ou en cliquant sur **LOG IN** en haut à droite de la page). En entrant une adresse mail inconnue de Framavox, vous serez invité à vous créer un compte.
  * cliquer sur **Connectez-vous avec votre adresse mail**
  * définir un nom
  * cliquer sur **Créez un compte**
  * entrer le code qui vous est envoyé par mail
  * cliquer sur **Se connecter**
  * [OPTIONNEL] ajouter une photo avatar (ou cliquer sur **X** pour fermer la fenêtre)

## Se connecter à son compte

Le fonctionnement par défaut de Loomio (le logiciel utilisé pour Framavox) est d'envoyer un mail avec un lien de connexion ou un code valable 24h et qui ne peut être utilisé qu'une seule fois (si cela ne fonctionne pas, il faut alors réessayer) quand vous entrez votre adresse mail sur la page d'accueil.

Pour modifier ce fonctionnement et paramétrer un mot de passe utilisable plusieurs fois, vous devez&nbsp;:

  1. cliquer sur <i class="fa fa-bars" aria-hidden="true"></i> en haut à gauche pour faire apparaître le menu latéral
  * cliquer sur votre adresse mail <i class="fa fa-caret-down" aria-hidden="true"></i>
  * cliquer sur **Modifier mon profil**
  * cliquer sur **Réinitialiser mon mot de passe**
  * saisir votre mot de passe deux fois
  * cliquer sur **Définir un mot de passe**

A la prochaine connexion il vous sera alors demandé le mot de passe que vous avez défini. Vous gardez néanmoins la possibilité de passer par l'envoi de mail pour vous connecter.

## Créer un nouveau groupe depuis la page d‎‎’accueil

Vous pouvez créer un nouveau groupe depuis la page d‎‎’accueil de votre compte Framavox en cliquant sur **Nouveau groupe +** dans le menu latéral gauche. Voir [Paramètres de groupe](group_settings.html).

## Vous êtes invité dans un groupe Framavox

Lorsque vous recevez une invitation à rejoindre un groupe Framavox existant, vous recevez un lien d’invitation unique par courriel.

Si c’est votre première utilisation de Framavox, il vous sera demandé de créer un compte. Si vous possédez déjà un compte, vous serez amené directement à ce groupe une fois authentifié.
