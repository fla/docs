# Sous-groupes

Framavox vous offre la possibilité de créé des sous-groupes, au sein de vos groupes. Les sous-groupes permettent de diviser votre groupe en différentes équipes. Les sous-groupes Framavox peuvent accueillir, ou non, des membres du groupe parent ou non. Selon les paramètres du groupe [paramètres du groupe](group_settings.html), les sous-groupes peuvent être créés soit par n‎‎’importe quel membre du groupe, ou bien seulement par les coordinateurs.

Contrairement aux tags, les sous-groupes créent un espace séparé pour un groupe spécifique de personnes. Les tags sont un moyen d'organiser vos fils de discussion. Les sous-groupes vous permettent de modifier les autorisations, les notifications, etc. pour un ensemble de discussions et de personnes.

Les sous-groupes fonctionnent comme les groupes, mais sont situés au sein de votre groupe/organisation, que nous appelons le "groupe parent". La plupart des fonctionnalités et des paramètres sont les mêmes que ceux que vous rencontrez dans votre groupe parent. Cela signifie également qu'une personne peut faire partie de votre sous-groupe (par exemple, votre conseil d'administration) mais pas de votre groupe parent.

## Ajouter un sous-groupe

Pour créer un sous-groupe vous devez, **depuis la page du groupe parent**&nbsp;:

  1. cliquer sur l'onglet **SOUS-GROUPES**
  * cliquer sur le bouton **NOUVEAU SOUS-GROUPE**
  * indiquer le nom du sous-groupe
  * paramétrer sa visibilité
  * cliquer sur **CRÉER UN SOUS-GROUPE**

## Visibilité du sous-groupe

Pour modifier les paramètres de votre sous-groupe vous devez cliquer sur **PARAMÈTRES** de votre page puis sur **Modifier les paramètres du groupe**

![image permission sous groupe loomio](images/loomio-sous-groupe-permissions.png)

Les paramètres pour les sous-groupes sont les mêmes que pour [les groupes](group_settings.html) mais avec des options supplémentaires.

Dans la partie **Confidentialité**  de l‎‎’onglet **Modifier les paramètres du groupe**, vous avez la possibilité entre&nbsp;:

  * Ouvert - Tout le monde peut trouver ce sous-groupe et demander à le rejoindre. Seuls les membres peuvent voir qui est dans le groupe. Toutes les discussions sont publiques.
  * Fermé - Tout le monde peut trouver ce sous-groupe et demander à le rejoindre. Seuls les membres peuvent voir qui est dans ce groupe. Toutes les discussions sont privées.
  * Secret - Seuls les membres invités peuvent trouver ce sous-groupe, voir les membres et voir les discussions.


![image confidentialité sous groupe loomio](images/loomio-sous-groupe-confidentialite.png)

Dans les permissions, le paramètre **Les membres de [SOUS-GROUPE] peuvent voir les discussions privées.** sera décoché par défaut pour la confidentialité **Fermé**.
