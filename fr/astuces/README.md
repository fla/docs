# Astuces

Cette section souhaite répondre aux questions générales liées à l'informatique. Pour des astuces relatives aux services que nous proposons, vous trouverez parfois une section **Astuces** dans leur documentation respective.

## Créer un lien dans un mail

Pour éviter des soucis de copié/collé des liens ou les modifications de ceux-ci par les logiciels mails, vous pouvez insérer un lien dans un mot ou une phrase.

### Avec Thunderbird

Pour ce faire, dans un message, vous devez sélectionner le mot ou la phrase en double-cliquant dessus ou en maintenant le clic, puis&nbsp;:

  * cliquer sur **Insérer**

  ![image montrant comment insérer un lien dans thunderbird](images/astuces_thunderbird_inserer_lien.gif)
  * entrer le lien qui sera lié au mot (ici **site**)

  ![image montrant les propriétés du lien à insérer dans Thunderbird](images/astuces_thunderbird_inserer_lien_proprietes.png)
  * cliquer sur **OK**

La personne recevant le mail aura le lien inséré directement dans le mot (visible dans la barre inférieure en survolant le mot)&nbsp;:

![image montrant un lien survolé](images/astuces_thunderbird_lien_insere.gif)

### Avec gmail

Pour ce faire, vous devez sélectionner le mot ou la phrase en double-cliquant dessus ou en maintenant le clic, puis&nbsp;:

  * cliquer sur l'icône «&nbsp;lien&nbsp;»

  ![image montrant l'icône pour créer un lien](images/astuces_gmail_lien.gif)

  * ajouter le lien à insérer sous **À quelle URL ce lien est-il associé ?**
  * cliquer sur **OK**

  ![image montrant comment insérer le lien dans la phrase sélectionnée](images/astuces_gmail_lien.png)

## Fournir les infos de débogage de DAVx5

Pour aider à comprendre un problème nous pouvons avoir besoin des informations fournies par l'application. Pour les obtenir vous devez&nbsp;:

  1. cliquer sur le menu <i class="fa fa-bars" aria-hidden="true"></i>
  * cliquer sur <i class="fa fa-cog" aria-hidden="true"></i> **Paramètres**
  * cliquer sur <i class="fa fa-bug" aria-hidden="true"></i> **Afficher les infos de débogage**

A partir de là, vous pouvez fournir les informations en cliquant sur l'icône de partage <i class="fa fa-share-alt" aria-hidden="true"></i> et sélectionner votre application mail ou copier les données et les coller dans un [Framabin](https://framabin.org/) et nous donner l'adresse de celui-ci.

## Vider le *localStorage*

### Vider le localstorage sur Firefox

Pour vider le *localStorage* d'un site, vous devez utiliser la console intégrée en étant sur celui-ci. Pour ce faire, vous devez&nbsp;:

  1. appuyer sur la touche `F12`
  * cliquer sur l'onglet **Stockage**
  * cliquer sur **Stockage local** dans le menu latéral gauche
  * faire un clic-droit sur le site pour lequel vous souhaitez supprimer le **localStorage**
  * cliquer sur **Tout supprimer**

![vider le localStorage Firefox](images/astuces-localstorage-firefox.png)

### Vider le localstorage sur Chrome / Chromium

Pour vider le *localStorage* d'un site, vous devez utiliser la console intégrée en étant sur celui-ci. Pour ce faire, vous devez&nbsp;:

  1. appuyer sur la touche `F12`
  * cliquer sur l'onglet **Application**
  * cliquer sur la flèche à côté de **Local Storage**
  * cliquer droit sur le site lequel vous souhaitez supprimer le **localStorage**
  * cliquer sur **Clear**

![vider le localStorage chrome](images/astuces-localstorage-chrome.png)

## Effacer des cookies

### Effacer des cookies sur Firefox

Pour effacer un cookie d'un site avec Firefox, vous devez&nbsp;:

  1. cliquer sur l'icône <i class="fa fa-info-circle" aria-hidden="true"></i> à côté de l'adresse du site
  * cliquer sur **Effacer les Cookies et les données de sites…**
  * cliquer sur le site dans la liste
  * cliquer sur le bouton **Supprimer**

![gif pour effacer cookie dans firefox](images/astuces-cookies-firefox.gif)

Voir aussi [la documentation de Firefox à ce sujet](https://support.mozilla.org/fr/kb/effacer-les-cookies-pour-supprimer-les-information#w_effacer-les-cookies-du-site-actuellement-visitae).

### Effacer des cookies sur Chrome

Pour effacer un cookie d'un site avec Chrome, vous devez&nbsp;:

  1. cliquer sur l'icône cadenas <i class="fa fa-lock" aria-hidden="true"></i> à côté de l'adresse du site
  * cliquer sur **Cookies**
  * cliquer sur le site dans la liste (ici `framadate.org`)
  * cliquer sur le bouton **Supprimer**
  * cliquer sur le bouton **OK**

![gif pour effacer cookie dans chrome](images/astuces_cookie_chrome.gif)

Voir aussi [la documentation de Chrome à ce sujet](https://support.google.com/chrome/answer/95647).

## Autoriser des cookies

Certains sites nécessitent que vous gardiez leur cookie pour être reconnu (par exemple Framapad pour savoir que vous êtes associé à une couleur spécifique). Si votre navigateur supprime les cookies à la fermeture, votre couleur ne sera plus associée à votre navigateur et vous devrez la modifier à chaque fois. Pour éviter cela, vous pouvez autoriser spécifiquement les cookies de Framapad (ou autres sites). Pour ce faire&nbsp;:

### Autoriser des cookies avec Firefox

Vous devez&nbsp;:

  1. cliquer sur le menu <i class="fa fa-bars" aria-hidden="true"></i>
  * cliquer sur <i class="fa fa-cog" aria-hidden="true"></i> **[Préférences](about:preferences)**
  * cliquer sur <i class="fa fa-lock" aria-hidden="true"></i> **Vie privée et sécurité**
  * cliquer sur **Gérer les exceptions** dans la section **Cookies et données de sites**
  * entrer ``framapad.org`` dans le champ **Adresse du site web**
  * cliquer sur le bouton **Autoriser**
  * cliquer sur le bouton **Enregistrer les modifications**

## Recherche dans l'historique de navigation

Pour retrouver un page web visitée précédemment vous pouvez regarder dans votre historique de navigation. Pour ce faire vous devez&nbsp;:

  1. maintenir la touche `Ctrl` et appuyer sur la touche `h` pour ouvrir le panneau de recherches
  * taper le nom de la page que vous cherchez (le nom de votre Framapad ou celui de votre Framacalc, par exemple)
  * cliquer sur ce titre dans la liste pour l'ouvrir à nouveau

## Zoomer et dézoomer une page web

Pour zommer (rendre l'affichage plus "gros"), ou dézoomer (le rendre plus "petit") vous devez maintenir la touche **ctrl** et utiliser la molette de la souris dans un sens ou dans l'autre.

Vous pouvez aussi utiliser la touche **ctrl** (en la maintenant enfoncée) et en utilisant la touche **+** pour zoomer ou **-** pour dézoomer.

<div class="alert-info alert">
Pour revenir à l'affichage standard vous devez maintenant la touche <b>ctrl</b> et appuyer sur le <b>0</b> (zéro).
</div>

<div class="alert-info alert">
Sur mac, il faut remplacer la touche <b>ctrl</b> par la touche <b>commande</b>.
</div>
