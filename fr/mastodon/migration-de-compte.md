# Migration de compte

Il n'est pas possible de modifier son identifiant mais il est possible de&nbsp;:

  * lier son ancien compte à son nouveau en mettant un bandeau sur l'ancien compte pour dire que le nouveau se trouve autre part

  ![image d'un compte déménagé](screenshots/mastodon_compte_demenagement.png)

  * importer sa&nbsp;:
    * Liste d’utilisateur⋅ice⋅s suivi⋅e⋅s
    * Liste d’utilisateur⋅ice⋅s bloqué⋅e⋅s
    * Liste d’utilisateur⋅ice⋅s que vous masquez
    * Liste des serveurs bloquées

Il **n'est pas** possible de récupérer ses pouets et médias téléversés de l'ancien vers le nouveau compte.

Pour opérer la migration, vous devez, depuis l'**ancien compte**&nbsp;:

  * aller dans [votre profil > **Import and export** > **Export des données**](https://framapiaf.org/settings/export)
  * cliquer sur les icônes <i class="fa fa-download" aria-hidden="true"></i> pour télécharger vos données relatives aux abonnements au format `CSV`
  * aller dans [**Profile**](https://framapiaf.org/settings/profile) > **Déplacer vers un compte différent** (en bas de la page) et cliquer sur [**Configurer ici**](https://framapiaf.org/settings/migration)
  * entrer l'adresse de votre nouveau profil
  * cliquer sur **Enregistrer**

Sur le nouveau compte, vous devez importer vos données relatives aux abonnements depuis votre profil > **import and export** > **Import des données** ([sur Framapiaf, par exemple](https://framapiaf.org/settings/import)) en sélectionnant dans la liste déroulante le fichier à importer puis en cliquant sur **Parcourir** pour récupérer le fichier précédemment téléchargé sur votre ordinateur.

<p class="alert alert-info">Vos anciens abonné⋅e⋅s devront se réabonner à votre nouveau compte.</p>
