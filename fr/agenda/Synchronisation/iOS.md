# Synchronisation sur iOS et iPadOS
[<i class="fa fa-arrow-left" aria-hidden="true"></i> Retour à l'accueil](../README.md)

## Agendas
* Ouvrez l'application **Réglages**.
* Sélectionnez **Calendrier**.
* Sélectionnez **Comptes**.
* Sélectionnez **Ajouter un compte**.
* Sélectionnez **Autre**.
* Sélectionnez **Ajouter un compte CalDAV**.
* Entrez les détails de votre compte.

    * Pour le serveur, saisissez simplement `framagenda.org`.
    * Entrez votre nom d'utilisateur et votre mot de passe.
    * Entrez ce que vous voulez comme description.
    * Sélectionnez **Suivant**.

Votre agenda sera maintenant synchronisé et visible dans les applications Calendrier et Rappels.

<p class="alert-info alert">
<b>Note</b>&nbsp;: Si vous utilisez <a href="../Inscription-Connexion.md#facultatif-utiliser-lauthentification-en-deux-tapes-2fa">l'authentification en deux étapes</a>, vous devez créer <a href="../Inscription-Connexion.md#utiliser-les-mots-de-passe-dapplication">un mot de passe d'application</a>.
</p>

<p class="alert-warning alert">
<b>Attention</b>&nbsp;: Si vous avez une espace dans votre nom d'utilisateur, la synchronisation ne pourra pas s'effectuer. Vous pouvez toutefois exporter vos données et vous recréer un nouveau compte sans cet espace dans le nom d'utilisateur en suivant <a href="https://contact.framasoft.org/fr/faq/#agenda-changement-identifiant">cette procédure</a>.
</p>

## Contacts
* Ouvrez l'application **Réglages**.
* Sélectionnez **Contacts**.
* Sélectionnez **Comptes**.
* Sélectionnez **Ajouter un compte**
* Sélectionnez **Autre**
* Sélectionnez **Ajouter un compte CardDAV**.
* De la même manière que pour les agendas, entrez les détails de votre compte.
* Si les informations sont correctes, sélectionnez **Terminé**.

Vos contacts seront maintenant synchronisés et visibles dans l'application Contacts.
