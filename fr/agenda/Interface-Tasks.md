# Utilisation de l'application Tâches
[<i class="fa fa-arrow-left" aria-hidden="true"></i> Retour à l'accueil](../README.md)

Vous pouvez accéder à l'application **Tâches** via le menu supérieur, en cliquant sur <i class="fa fa-check" aria-hidden="true"></i>.
L'application contient la liste de vos listes de tâches et agendas sur la partie gauche et les tâches pour chaque liste sur la partie droite.

![Vue de l'application Tâches](images/tasks-1.png)

## Gestion des tâches
### Création d'une tâche
Pour créer une nouvelle tâche, cliquez sur la zone de texte **Ajouter une tâche dans "[nom de la liste]"…** et entrez le nom de votre nouvelle tâche.

Les détails de la tâche sont accessibles et éditables en cliquant dessus.

![Vue de l'édition d'une tâche](images/tasks-ajout.gif)

Les informations entrées sont automatiquement sauvegardées dès qu'elles sont entrées, il n'y a donc pas de bouton pour valider la saisie..

### Visualisation et édition des détails d'une tâche

Cliquez sur une tâche pour y accéder et éditer ses informations. De la même manière que lors de la création, les informations sont automatiquement sauvegardées dès lorsqu'elles sont modifiées.

![Vue de l'édition d'une tâche](images/tasks-2.png)

Il est possible de créer une sous-tâche à partir d'une tâche en cliquant sur le petit **…** puis sur **+ Ajouter une sous-tâche**. Les sous-tâches peuvent à leur tour posséder des sous-tâches, et ainsi de suite.
Il est également possible de marquer des sous-tâches comme favorites afin de les retrouver plus facilement en cliquant sur <i class="fa fa-star" aria-hidden="true"></i>.
Enfin, une tâche peut être marquée comme validée.

![gif de la création d'une sous tâche](images/tasks-ajout-sous-tache.gif)

## Gestion des catégories

Les tâches sont automatiquement classées dans des catégories (comme **Aujourd'hui**, **Cette semaine**, **En ce moment** ou **Terminé**) en fonction de leurs informations ou leur statut.

## Gestion des listes de tâches

Vous pouvez créer une nouvelle liste de tâches en cliquant sur **Ajouter une liste...** en dessous de la liste des listes de tâches. Une fois sélectionnée, chaque liste de tâche donne accès à plusieurs actions.

### Édition

Une liste de tâche possède un nom et une couleur. Ces propriétés sont éditables.

### Export

Les tâches sont contenues dans l'export de l'agenda associé et cette liste de tâches sera téléchargée au format `vcs/ics`.

### Lien CalDAV

Ce lien peut être utile pour la synchronisation d'une liste de tâches unique, à la manière d'un agenda.

### Suppression de la liste de tâches

La suppression d'une liste de tâches supprime également toutes les tâches lui étant associée.
