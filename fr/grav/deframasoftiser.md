# Déframasoftiser Framasite «&nbsp;Blog&nbsp;»

Afin de [déframasoftiser Internet](https://framablog.org/2019/09/24/deframasoftisons-internet/) voici comment exporter vos données depuis nos services.

<div class="alert-warning alert">
<b>Attention</b>, cette procédure ne concerne pas les sites «&nbsp;une page&nbsp;»&nbsp;! Si vous voyez la mention «&nbsp;Hébergé par Framasoft. Créé grâce à NoemieCMS.&nbsp;» tout en bas de votre site, celui-ci <b>n'est</b> malheureusement <b>pas</b> exportable. <br />
Nous pouvons vous fournir une archive de celui-ci mais il ne sera utilisable que chez un hébergeur utilisant le logiciel NoémieCMS et nous n'en avons identifié aucun pour le moment.
</div>

## Export du site

Pour exporter votre site, vous devez&nbsp;:
  1. aller sur la page **Tableau de bord**
  * cliquer sur le bouton **Sauvegarder**
  * cliquer sur **Télécharger la sauvegarde** pour récupérer l'archive sur votre ordinateur

![image de la partie sauvegarde de Grav](images/deframasoftiser-grav.png)
