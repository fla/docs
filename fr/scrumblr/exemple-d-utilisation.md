# Exemple d'utilisation de <b class="violet">Fra</b><b class="vert">memo</b>

Lorsque l’on est en réunion, que l’on travaille en groupe, il faut
prendre des notes. Pour cela, un [Framapad](https://framapad.org), c’est
plutôt pratique. Mais dès que vous voulez organiser ces notes, on en
revient au tableau blanc avec les sempiternels papiers jaunes à bouts
collants ([dont on ne doit pas prononcer le
nom](https://www.inpi.fr/fr/valoriser-vos-actifs/le-mag/3m-company-empecher-la-degenerescence-de-marque))…

<span id="more-7162"></span>

Du coup, nous on s’est dit « Ce serait-y pas formidable qu’un logiciel
libre permette de simuler le tableau blanc et les papiers, de manière
collaborative, comme les pads ? »

Et c’est là que nous avons découvert
[Scrumblr](https://github.com/aliasaria/scrumblr), de Ali Asaria ; qui a
eu la belle idée de créer un logiciel compréhensible instinctivement, en
deux clics. La suite, vous la devinez…

[Framemo](https://framemo.org) expliqué aux kanbanistes agiles
--------------------------------------------------------------

Oui, le tableau blanc, les colonnes et les papiers repositionnables,
cela s’appelle du
[Kanban](https://fr.wikipedia.org/wiki/Kanban_(d%C3%A9veloppement)). Le
passage qui suit est donc à réserver aux personnes qui connaissent déjà
la méthode (ou veulent un bref aperçu de ce service). Si vous voulez une
illustration explicative, pas de soucis, rendez-vous au titre
suivant ;) !

[Framemo](https://framemo.org) vous propose donc :

-   De créer des tableaux kanban en ligne (sans ouverture de compte) ;
-   De les partager via l’URL (adresse web) ;
-   De travailler collaborativement en temps réel ;
-   Le tout en mode cliquer-glisser-déposer ;
-   L’édition, l’ajout et la suppression de colonnes ;
-   L’édition, l’ajout et la suppression de notes (4 couleurs dispos) ;
-   L’ajout ou le retrait de gommettes sur les notes ;
-   L’ajout et l’édition du pseudonyme des participant-e-s ;

Ce service est basé sur le logiciel [Scrumblr (participez-y
ici)](https://github.com/aliasaria/scrumblr), sous licence [GNU
GPL](https://github.com/aliasaria/scrumblr/blob/master/LICENSE.txt), et
notre tuto pour l’installer sur vos serveurs est [sur le
Framacloud](http://framacloud.org/cultiver-son-jardin/installation-de-scrumblr/).

L’initiative Cot-Cot-Commons ouvre un [Framemo](https://framemo.org)
--------------------------------------------------------------------

Prenons l’exemple (purement fictionnel) de Cot-Cot-Commons, une
organisation qui voudrait créer un prototype de poulailler Libre (en
plus d’être *open source*) et auto-géré, dans le jardin partagé de
l’immeuble de Sandrine. Cette dernière s’y investit (elle aime réduire
ses déchets végétaux) mais voit très vite que les idées fusent en
réunion sans être forcément notées pour les personnes n’ayant pas pu
venir. Qu’à cela ne tienne, lors de la réunion suivante, elle ouvre un
Framemo !

Elle se rend donc sur [Framemo.org](https://framemo.org) et décide de
créer un tableau sobrement intitulé
« [CotCotCommons](https://framemo.org/CotCotCommons)« . Pas besoin de
créer un compte, il suffit juste de taper le nom de son tableau et de
cliquer sur « Allons-y » !

![01-memo-creation](images/01-memo-creation.png)

Bon, pour l’instant, il faut avouer que ce n’est pas super entraînant :
un tableau tout vide, et quelques boutons « plus » et « moins » ici ou
là, de petits points colorés…

![02-memo-tableau-vide02](images/02-memo-tableau-vide02.png)

En passant sa souris sur la droite du tableau, Sandrine fait apparaître
un petit « plus » qui lui permet d’ajouter des colonnes. Lorsqu’elle
clique sur leur titres, elle s’aperçoit qu’elle peut en modifier le nom.
Sandrine prépare un tableau kanban assez classique (de toutes façons
elle voit bien qu’on peut toujours modifier les noms de colonnes par la
suite).

![03-memo-colonnes](images/03-memo-colonnes.png)

Notons au passage que Sandrine a vu qu’il suffisait de cliquer sur
« Anonyme (vous) » pour entrer son prénom (ou son pseudo). Étant une
habituée des pads, elle sait combien c’est pratique et met vite le sien.

Puis elle commence à noter les idées du compte rendu de la séance
précédente. Pour créer une note, elle clique sur le « plus » à gauche
sous le tableau. Un petit papier apparaît qui ne demande qu’à être
édité, puis déplacé d’un geste de la souris…

![04-memo-1ere-note](images/04-memo-1ere-note.png)

Sandrine n’y tient plus, elle finit juste quelques notes avant que de
partager l’adresse web du tableau avec le reste de Cot-Cot-Commons. Elle
est facile à retenir c’est le nom du site puis le nom de son tableau :
[framemo.org/CotCotCommons](https://framemo.org/CotCotCommons)

![05-memo-debut](images/05-memo-debut.png)

Très vite, Abdel, le secrétaire de séance, décide de projeter le tableau
sur le mur de la salle de réunion. Dans le groupe, plusieurs s’emparent
de leurs ordinateurs (dont Naya, restée chez elle à cause d’une jambe
cassée, qui participe donc en visio conf) pour réorganiser les notes de
Sandrine et y ajouter leurs idées !

C’est d’ailleurs Naya qui découvre qu’on peut glisser et déposer sur
chacune des notes les gommettes qui se trouvent en bas à droite. Le
groupe décide donc d’un code couleur pour attribuer les tâches à
chacun-e. Bien vite, il y a tant de notes qu’il leur faut agrandir le
tableau (en le tirant vers le bas d’un simple geste de la souris !)

[![06-memo-final](images/06-memo-final.png)

 

À vous de tester (et partager) [Framemo](https://framemo.org) !
---------------------------------------------------------------

Et voilà : Ali Asaria (et toute l’équipe de
[Scrumblr](https://github.com/aliasaria/scrumblr)) nous propose là un
petit service efficace, pratique, facile d’accès… L’idéal pour
collaborer aisément sans avoir à apprendre et à s’habituer à un logiciel
plus complexe !

Car si le collectif Cot-Cot-Commons est une pure invention, nous sommes
certains que Framemo peut être utile à vos associations, syndicats,
familles, entreprises, institutions : bref, dans tous les cercles où
vous vous organisez en commun.

Et si vous trouvez Framemo trop léger pour vos besoins, nous vous
rappelons qu’il existe un outil plus orienté gestion de projets – et
donc plus complexe – nommé [Framaboard.org](https://framaboard.org)
utilisant lui aussi la méthode kanban.

Nous sommes ravis de pouvoir vous proposer ce nouveau service, et
attendons de voir avec impatience si vous allez vous en emparer,
l’utiliser, le partager… et surtout le quitter parce que vous l’aurez
finalement [adopté sur vos
serveurs](http://framacloud.org/cultiver-son-jardin/installation-de-scrumblr/) ;) !

### Pour aller plus loin :

-   Tester [Framemo](https://framemo.org)
-   Découvrir [Scrumblr](https://github.com/aliasaria/scrumblr)
-   [Installer
    Scrumblr](http://framacloud.org/cultiver-son-jardin/installation-de-scrumblr/)
    sur vos serveurs
