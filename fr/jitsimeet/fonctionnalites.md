# Fonctionnalités

## Afficher son nom
  1. En haut à droite, passer la souris sur la miniature de votre webcam,
  2. Double-cliquer sur le nom afficher, et entrer ce que vous voulez !
   ![Modifier son nom](images/screen005.png)

## Changer d'affichage

  1. En bas à droite, cliquer sur le bouton en forme de petits carrés,
   ![Passer en affichage Mosaic](images/screen006.png)
   ![Affichage Mosaic (Tous les participants)](images/screen007.png)

  2. Pour afficher un écran en particulier, cliquer simplement sur la vidéo que vous souhaitez voir en grand.

## Mettre un mot de passe

Pour rendre privé un salon vous pouvez lui donner un mot de passe. Tout personne arrivant sur le salon protégé par un mot de passe devra le taper pour accéder.

Pour rendre un salon privé, vous devez&nbsp;:

  1. passer la souris dans le coin inférieur droit pour faire apparaître <i class="fa fa-shield" aria-hidden="true"></i>
  * cliquer sur **Ajouter un mot de passe**
  * taper le mot de passe
  * appuyer sur la touche **Entrée**

![gif de comment entrer un mot de passe](images/jitsi-mdp.gif)

Vous pouvez ensuite copier ou supprimer ce mot de passe.

<div class="alert-info alert">
L'icône <i class="fa fa-shield" aria-hidden="true"></i> est orange quand il n'y a pas de mot de passe et verte quand il y en a un.
</div>

## Partage d'écran

Pour partager votre écran ou une fenêtre en particulier, vous devez :

  1. Cliquer sur l'icône **écran** en bas à gauche de l'écran ![Bouton Écran](images/screen010.png)

  2. Sélectionner soit :

  * votre écran entier,
  * une fenêtre en particulier,
  * un onglet de votre navigateur.

    ![Bouton Écran](images/screen011.png)


  3. Cliquer sur **Share**.

Pour ne plus partager l'écran, vous devez cliquer à nouveau sur le bouton **écran**.

## Changer la langue

Pour changer la langue, vous devez&nbsp;:

  1. déplacer votre souris en bas de l'écran
  * cliquer sur <i class="fa fa-ellipsis-v" aria-hidden="true"></i> (**1**)
  * cliquer sur **Paramètres**

  ![Framatalk menu image](images/framatalk_menu.png)
  * cliquer sur **Plus**
  * changer **Langue**

  ![Framatalk paramètres plus image](images/Framatalk_parametres_plus.png)
  * cliquer sur **Ok**

## Activer/désactiver la caméra

Si vous n'avez pas besoin de la caméra et/ou pour gagner de la bande passante vous pouvez la désactiver. Pour ce faire vous devez cliquer sur <i class="fa fa-video-camera" aria-hidden="true"></i>. Pour la réactiver vous devez simplement appuyer à nouveau sur ce même bouton.

![icône caméra de jitsi](images/framatalk-cam-on-off.gif)



## Activer/désactiver le micro

  * Pour couper votre micro vous devez cliquer sur <i class="fa fa-microphone" aria-hidden="true"></i>.
  * Pour le réactiver, vous devez cliquer sur <i class="fa fa-microphone-slash" aria-hidden="true"></i>
