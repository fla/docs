# Framasite

<div class="alert-warning alert">
<b>Déframasoftisons Internet</b> : l’association Framasoft a programmé la fermeture de certains services, pour éviter d’épuiser nos maigres ressources et pour favoriser l’utilisation de petits hébergeurs de proximité (<a href="https://framablog.org/2020/03/03/10-bonnes-raisons-de-fermer-certains-services-framasoft-la-5e-est-un-peu-bizarre/">toutes les infos ici</a>).
Framasite a fermé <b>mardi 6 juillet 2021</b>.
Nous avons réuni des outils pour vous aider à récupérer vos données et à trouver un service similaire <a href="https://alt.framasoft.org/fr/framasite">sur cette page</a>.
</div>

L'interface de <b class="violet">Frama</b><b class="vert">site</b> permettant de gérer vos sites et vos domaines est développée par Framasoft et disponible sur [notre dépôt Framagit](https://framagit.org/framasoft/Framasite-/framasite), sous licence [AGPL-3.0](https://framagit.org/framasoft/Framasite-/framasite/blob/master/LICENSE).

---

## Pour aller plus loin&nbsp;:

- Utiliser [Framasite](https://frama.site/)
- [Prise en main](prise-en-main.html)
- [Rattacher son nom de domaine à un site ou un wiki](rattachement-nom-de-domaine.html)
- [Contribuez au code](https://framagit.org/framasoft/Framasite-/framasite)
- Un service proposé dans le cadre de la campagne [contributopia](https://contributopia.org/)
- [Soutenir Framasoft](https://soutenir.framasoft.org)
